﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Runtime.Serialization;

namespace CS481_HW3_Hierarchy
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
