﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3_Hierarchy
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page2 : ContentPage
    {
        char[] enteredText;
        char[] reversedChars;

        public Page2()
        {
            InitializeComponent();
        }

        void Handle_Page2Appearing(object sender, EventArgs e)
        {
            enteredText = new char[] { };
            reversedChars = new char[] { };
        }

        void Handle_Page2Disappearing(object sender, EventArgs e)
        {
            reverseStack.Children.Clear();
        }

        void Handle_ReverseTextFieldEntered(Object sender, EventArgs e)
        {
            enteredText = ((Entry)sender).Text.ToCharArray(0, ((Entry)sender).Text.Length);
            reversedChars = new char[enteredText.Length];
            int j = enteredText.Length;
            foreach(char i in enteredText)
            {
                j--;
                reversedChars[j] = i;
            }
            string reversedString = new string(reversedChars);
            Label newReversedText = new Label { Text = reversedString };
            reverseStack.Children.Add(newReversedText);
            entryToBeReversed.Text = "";
            Array.Clear(enteredText, 0, enteredText.Length);
            Array.Clear(reversedChars, 0, reversedChars.Length);
            reverseScroll.ScrollToAsync(reverseStack, ScrollToPosition.End, true);
        }

        async void NextPage_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page3());
        }

        async void PreviousPage_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }

        async void BackToMain_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopToRootAsync();
        }
    }
}