﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3_Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        string enteredText;

        public Page1()
        {
            InitializeComponent();
        }

        void Handle_Page1Appearing(object sender, EventArgs e)
        {
            enteredText = "";
        }

        void Handle_Page1Disappearing(object sender, EventArgs e)
        {
            textStack.Children.Clear();
            enteredText = "";
        }
       
        void Handle_TextFieldEntered(object sender, EventArgs e)
        {
            enteredText = ((Entry)sender).Text;
            Label newText = new Label { Text = enteredText };
            textStack.Children.Add(newText);
            entryText.Text = null;
            textScroll.ScrollToAsync(textStack, ScrollToPosition.End, true);
        }

        async void NextPage_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page2());
        }

        async void PreviousPage_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}