﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

//https://stackoverflow.com/questions/44067764/converting-random-to-int-c-sharp

namespace CS481_HW3_Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3 : ContentPage
    {
        string enteredTextToColor;
        int randomNumberConverted;
        Random randomNumber;

        public Page3()
        {
            InitializeComponent();
        }

        void Handle_Page3Appearing(object sender, EventArgs e)
        {
            enteredTextToColor = "";
            randomNumberConverted = 0;
            randomNumber = new Random();
        }

        void Handle_Page3Disappearing(object sender, EventArgs e)
        {
            colorStack.Children.Clear();
            enteredTextToColor = "";
            randomNumberConverted = 0;
            randomNumber = null;
        }

        void Handle_ColorTextFieldEntered(object sender, EventArgs e)
        {
            enteredTextToColor = ((Entry)sender).Text;
            randomNumberConverted = randomNumber.Next(0, 8);
            Label newColoredText = new Label { Text = enteredTextToColor };
            switch (randomNumberConverted)
            {
                case 0:
                    newColoredText.TextColor = Color.Orange;
                    break;
                case 1:
                    newColoredText.TextColor = Color.Red;
                    break;
                case 2:
                    newColoredText.TextColor = Color.Green;
                    break;
                case 3:
                    newColoredText.TextColor = Color.Blue;
                    break;
                case 4:
                    newColoredText.TextColor = Color.Coral;
                    break;
                case 5:
                    newColoredText.TextColor = Color.HotPink;
                    break;
                case 6:
                    newColoredText.TextColor = Color.Indigo;
                    break;
                case 7:
                    newColoredText.TextColor = Color.Olive;
                    break;
                case 8:
                    newColoredText.TextColor = Color.Yellow;
                    break;

            }
            colorStack.Children.Add(newColoredText);
            entryBeforeColor.Text = null;
            colorScroll.ScrollToAsync(colorStack, ScrollToPosition.End, true);
        }
        async void PreviousPage_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }

        async void BackToMain_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopToRootAsync();
        }
    }
}